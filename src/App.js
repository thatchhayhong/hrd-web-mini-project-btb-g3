import "bootstrap/dist/css/bootstrap.min.css";
import React, { useState } from "react";
import MyNavComponent from "./components/MyNavComponent";
import FormComponent from "./components/FormComponent";

function App() {
  const [brand, setBrand] = useState({
    name: "Personal Info React Bootstrap",
    signedName: "BTB-Group3",
  });

  return (
    <div className="App">
      <MyNavComponent brand={brand} />
      <br></br>
      <FormComponent />
    </div>
  );
}

export default App;
