import { Container, Row, Col, Button } from "react-bootstrap";
function FormCard({ onSwitch, viewAs }) {
  return (
    <>
      <div>
        <Container>
          <h3>Display Data As:</h3>
          <Row>
            <Col md={12} className=" mt-2">
              <Button
                onClick={() => onSwitch(true)}
                variant={viewAs ? "primary" : "secondary"}
                type="button"
              >
                Table
              </Button>
              <Button
                onClick={() => onSwitch(false)}
                variant={viewAs ? "secondary" : "primary"}
                type="button"
                active
              >
                Card
              </Button>
             
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
}

export default FormCard;
