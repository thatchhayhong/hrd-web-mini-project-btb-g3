import React, { useState, useEffect } from "react";
import { Container, Col, Row, Form, Button } from "react-bootstrap";
import TableList from "./TableList";
import { v4 as uuidv4 } from "uuid";
import FormCard from "./FormCard";
import CardList from "./CardList";
function FormComponent() {
  const [personalInfo, setPersonalInfo] = useState({
    data: [
      {
        id: "40e37e8e",
        username: "That chhayhong",
        email: "thatchhayhong@gmail.com",
        gender: "male",
        job: ["teacher"],
        created_date: 1622116399127,
        updated_date: 1622116399127,
      },
      {
        id: "40esdfe8e",
        username: "Leng Chhinghor",
        email: "lengchhinghor@gmail.com",
        gender: "male",
        job: ["developer"],
        created_date: 1622116399127,
        updated_date: 1622116399127,
      },
      {
        id: "40e3dfd8e",
        username: "Kay keo",
        email: "kaykeo@gmail.com",
        gender: "male",
        job: ["student"],
        created_date: 1622116399127,
        updated_date: 1622116399127,
      },
    ],
  });
  const [profile, setProfile] = useState({
    id: uuidv4().slice(0, 8),
    username: "",
    email: "",
    gender: "male",
    job: ["student"],
    created_date: Date.now(),
    updated_date: Date.now(),
    beingUpdate: false,
  });
  const [validate, setValidate] = useState({
    email: null,
    username: null,
  });
  const handleChange = (e) => {
    const { name, value } = e.target;
    setProfile((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleRadioButton = (e) => {
    const { name, id } = e.target;
    setProfile((prevState) => ({
      ...prevState,
      [name]: id,
    }));
  };
  const handleCheckBox = (e) => {
    const { name, id } = e.target;
    let getJobArray = [...profile.job];
    let findExistJob = getJobArray.includes(id);
    if (findExistJob) {
      getJobArray = getJobArray.filter((e) => e !== id);
      console.log(getJobArray);
      setProfile((prevState) => ({
        ...prevState,
        [name]: getJobArray,
      }));
    } else {
      setProfile((prevState) => ({
        ...prevState,
        [name]: [...profile.job, id],
      }));
    }
  };
  let emailPattern = /^\S+@\S+\.[a-z]{3}$/g;
  let emailValid = emailPattern.test(profile.email.trim());
  let usernamePattern = /^[^'"][a-zA-Z\s]*$/;
  let usernameValid = usernamePattern.test(profile.username);
  const onSave = () => {
    setValidate((prevState) => ({
      ...prevState,
      email: emailValid,
      username: usernameValid,
    }));
    if (validate.email && validate.username) {
      let data = personalInfo.data;
      data.push(profile);
      setProfile((prevState) => ({
        ...prevState,
        id: uuidv4().slice(0, 8),
      }));
      setPersonalInfo((prevState) => ({
        ...prevState,
        data: data,
      }));
      onResetForm();
    }
  };
  const onResetForm=()=>{
    setProfile({
      id: uuidv4().slice(0, 8),
      username: "",
      email: "",
      gender: "male",
      job: ["student"],
      created_date: Date.now(),
      updated_date: Date.now(),
    });
  }
  const [switchView, setswitchView] = useState(true);
  const onSwitch = (value) => {
    setswitchView(value);
  };
  const [updateIndex,setUpdateIndex]=useState({
    dataIndex:""
  })
  const onEdit = (data, index) => {
    let mydata = [...personalInfo.data];
    setUpdateIndex({
      dataIndex:index
    })
    let filterData = mydata.filter((m) => m.id == data.id);
    setProfile((prevState) => ({
      ...prevState,
      username: data.username,
      email: data.email,
      job: data.job,
      id: data.id,
      created_date: data.created_date,
      updated_date: Date.now(),
      beingUpdate: true,
    }));
  };
  const onDelete = (data, index) => {
    let mydata = [...personalInfo.data];
    let filterData = mydata.filter((m) => m.id !== data.id);
    setPersonalInfo((prevState) => ({
      ...prevState,
      data: filterData,
    }));
    console.log(filterData,mydata,data.id)
  };

  const onSaveUpdate=()=>{
    let data =profile
    let mydata = [...personalInfo.data];
    setProfile((prevState) => ({
      ...prevState,
      username: data.username,
      email: data.email,
      job: data.job,
      id: data.id,
      created_date: data.created_date,
      updated_date: Date.now(),
      beingUpdate: true,
    }));
    mydata[updateIndex.dataIndex].id = data.id;
    mydata[updateIndex.dataIndex] = profile;
    console.log(mydata, data);
    setPersonalInfo((prevState) => ({
      ...prevState,
      data: mydata,
    }));
    onResetForm();
  }
  const onCancel = () => {
    console.log("onCancel");
    setProfile((prevState) => ({
      ...prevState,
      beingUpdate: false,
    }));
    onResetForm();
  };
  return (
    <>
      <div>
        <Container>
          <Row>
            <Col sm={8}>
              <h4>Personal Infor</h4>
              <Form>
                <Form.Group controlId="formUsername">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter username"
                    value={profile.username}
                    type="text"
                    onChange={handleChange}
                    name="username"
                  />
                  <small id="emailHelp" className="form-text text-muted">
                    <span>
                      {validate.username === ""
                        ? "Username cannot be empty!"
                        : validate.username === null || validate.username
                        ? "Enter username"
                        : "Username Invalid"}
                    </span>
                  </small>
                </Form.Group>

                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={profile.email}
                    onChange={handleChange}
                    name="email"
                  />
                  <small id="emailHelp" className="form-text text-muted">
                    <span>
                      {validate.email === ""
                        ? "Email cannot be empty!"
                        : validate.email === null || validate.email
                        ? "Enter email"
                        : "Email Invalid"}
                    </span>
                  </small>
                </Form.Group>
                <div className="d-flex p-2">
                  <div className="mr-auto"></div>
                  {profile.beingUpdate ? (
                    <Button
                      className="mr-2"
                      variant="primary"
                      onClick={() => onSaveUpdate()}
                      type="button"
                    >
                      Update
                    </Button>
                  ) : (
                    <Button
                      className="mr-2"
                      variant="primary"
                      onClick={() => onSave()}
                      type="button"
                    >
                      Save
                    </Button>
                  )}
                  <Button
                    variant="secondary"
                    type="button"
                    onClick={() => onCancel()}
                  >
                    Cancel
                  </Button>
                </div>
              </Form>
            </Col>
            <Col sm={4}>
              <div>
                <h4>Gender</h4>
                <Form.Check
                  custom
                  inline
                  label="Male"
                  type="radio"
                  id="male"
                  name="gender"
                  value={profile.gender}
                  onChange={handleRadioButton}
                  defaultChecked={profile.gender === "male" ? true : false}
                />
                <Form.Check
                  custom
                  inline
                  label="Female"
                  type="radio"
                  id="female"
                  name="gender"
                  value={profile.gender}
                  onChange={handleRadioButton}
                  defaultChecked={profile.gender === "female" ? true : false}
                />
              </div>
              <h4>Job</h4>
              <div className="d-flex p-2">
                <Form.Group controlId="student" className="p-1">
                  <Form.Check
                    checked={profile.job.includes("student")}
                    value={profile.job}
                    onChange={handleCheckBox}
                    type="checkbox"
                    name="job"
                    id="student"
                    label="student"
                  />
                </Form.Group>
                <Form.Group controlId="teacher" className="p-1">
                  <Form.Check
                    checked={profile.job.includes("teacher")}
                    value={profile.job}
                    onChange={handleCheckBox}
                    type="checkbox"
                    name="job"
                    id="teacher"
                    label="teacher"
                  />
                </Form.Group>
                <Form.Group controlId="developer" className="p-1">
                  <Form.Check
                    checked={profile.job.includes("developer")}
                    value={profile.job}
                    onChange={handleCheckBox}
                    type="checkbox"
                    id="developer"
                    name="job"
                    label="developer"
                  />
                </Form.Group>
              </div>
            </Col>
          </Row>
        </Container>
        <FormCard onSwitch={onSwitch} viewAs={switchView} />
        <Container>
          <Row sm={12}>
            {switchView ? (
              <TableList data={personalInfo.data} onEdit={onEdit} onDelete={onDelete} />
            ) : (
              <CardList data={personalInfo.data} onEdit={onEdit} onDelete={onDelete}/>
            )}
          </Row>
        </Container>
      </div>
    </>
  );
}
const invalidText = {
  color: "red",
};
export default FormComponent;
