import React,{useState} from "react";
import { Col, Card, Dropdown,Row,Modal,Button } from "react-bootstrap";
import moment from "moment";
import localization from 'moment/locale/km';
moment.updateLocale('km', localization);
export default function CardList({ data, onEdit,onDelete }) {
  const [show, setShow] = useState(false);
  const [display,setDisplay]=useState({
    data:[]
  })
  const handleClose = () => setShow(false);
  const handleShow = (data) => {
    setShow(true)
    setDisplay((prevState) => ({
      ...prevState,
      data: data,
    }));
  };
  return (
    <Col>
      <Row md={4}>
        {data.map((item, index) => (
          <Card className=" mt-2" key={item.id}>
            <Card.Header as="h5">
              <Dropdown className="text-center">
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                  Action
                </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item onClick={()=>handleShow(item)}>View</Dropdown.Item>

                {show? <Modal 
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered  show={show} onHide={handleClose}>
                  <Modal.Header closeButton>
                    <Modal.Title>Information</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>
                  <b>Uesrname:</b> <span>{display.data.username}</span>
                  <br></br>
                  <b>Gmail:</b><span>{display.data.email}</span> 
                  <br></br>
                  <b>Job:</b><ul className="text-left">
                    {display.data.job.map((item,index)=>(
                      <li key={index}>{item}</li>
                    ))}
                  </ul>
                  <br></br>
                  <b>Created:</b> <i>{moment(display.data.created_date).fromNow()}</i>
                  </Modal.Body>
                  <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                      Close
                    </Button>
                  </Modal.Footer>
                </Modal>:""}

                <Dropdown.Item onClick={()=>onEdit(item,index)} >Update</Dropdown.Item>
                <Dropdown.Item onClick={()=>onDelete(item,index)} >Delete</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </Card.Header>
            <Card.Body>
              <Card.Title>{item.username}</Card.Title>
              job:
              <br></br>
              <ul>
                {item.job.map((job, index) => (
                  <ul key={index}>
                    <li>{job}</li>
                  </ul>
                ))}
              </ul>
            </Card.Body>
            <Card.Footer className="text-muted">
              {moment(item.created_date).fromNow()}
            </Card.Footer>
          </Card>
        ))}
      </Row>
    </Col>
  );
}
