import React, { Component } from "react";
import { Navbar, Image } from "react-bootstrap";

class MyNavComponent extends Component {
  render() {
    return (
      <div>
        <Navbar bg="dark text-white" expand="lg">
          <div className="container">
            <Navbar.Brand href="#home">
              <h3 className="text-white">{this.props.brand.name}</h3>
            </Navbar.Brand>
            <div className="mr-auto"></div>
            <Navbar.Brand href="#home">
              <h3 className="text-white">{this.props.brand.signedName}</h3>
            </Navbar.Brand>
          </div>
        </Navbar>
      </div>
    );
  }
}

export default MyNavComponent;
