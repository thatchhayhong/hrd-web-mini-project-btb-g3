import React, { useState, useEffect } from "react";
import { Col, Button, Table, Pagination, Modal, Form } from "react-bootstrap";
import moment from "moment";
import localization from "moment/locale/km";
moment.updateLocale("km", localization);
export default function TableList(props) {
  let active = 2;
  let items = [];
  for (let number = 1; number <= props.data.length / 2; number++) {
    items.push(
      <Pagination.Item
        key={number}
        // active={number === items}
        onClick={() => onPagination(number)}
      >
        {number}
      </Pagination.Item>
    );
  }
  // if use pagination update by index error.
  const [display, setDisplay] = useState({
    data: [],
  });
  useEffect(() => {
    let initiateValue = props.data.slice(0, 2);
    setDisplay({
      data: initiateValue,
    });
  }, []);
  let onPagination = (page) => {
    let prevPage = page - 1;
    let nextPage = prevPage + 2;
    let paginated = props.data.slice(prevPage, nextPage);
    setDisplay({
      data: paginated,
    });
  };
  const [show, setShow] = useState(false);
  const [displayDialog, setDisplayDialog] = useState({
    data: [],
  });
  const handleClose = () => setShow(false);
  const handleShow = (data) => {
    setShow(true);
    setDisplayDialog((prevState) => ({
      ...prevState,
      data: data,
    }));
  };
  let [showPagination, setPagination] = useState(false);
  const onViewPagination=()=>{
    let myBoolean=showPagination=!showPagination
    console.log(myBoolean)
    setPagination((prevState)=>(myBoolean))
  }
  return (
    <Col>
      <Form.Group controlId="formBasicCheckbox">
        <Form.Check type="checkbox" label="Pagination" onClick={()=>onViewPagination()} defaultChecked={showPagination}/>
      </Form.Group>
      <Table style={{ fontSize: 13 }} striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Gender</th>
            <th>Email</th>
            <th>Job</th>
            <th>Created</th>
            <th>Updated</th>
            <th>Action</th>
          </tr>
        </thead>

        {showPagination ? (
          <tbody>
            {display.data.map((item, index) => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.username}</td>
                <td>{item.gender}</td>
                <td>{item.email}</td>
                <td>
                  {item.job.map((job, index) => (
                    <ul key={index}>
                      <li>{job}</li>
                    </ul>
                  ))}
                </td>
                <td>{moment(item.created_date).fromNow()}</td>
                <td>{moment(item.updated_date).fromNow()}</td>
                <td>
                  <Button variant="primary" onClick={() => handleShow(item)}>
                    View
                  </Button>{" "}
                  {show ? (
                    <Modal
                      size="lg"
                      aria-labelledby="contained-modal-title-vcenter"
                      centered 
                      show={show}
                      onHide={handleClose}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Information</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <b>Uesrname:</b> <span>{displayDialog.data.username}</span>
                        <br></br>
                        <b>Gmail:</b><span>{displayDialog.data.email}</span> 
                        <br></br>
                        <b>Job:</b><ul className="text-left">
                          {displayDialog.data.job.map((item,index)=>(
                            <li key={index}>{item}</li>
                          ))}
                        </ul>
                        <br></br>
                        <b>Created:</b> <i>{moment(displayDialog.data.created_date).fromNow()}</i>
                        </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  ) : (
                    ""
                  )}
                  <Button
                    variant="info"
                    disabled={showPagination}
                    onClick={() => props.onEdit(item, index)}
                  >
                    Update
                  </Button>{" "}
                  <Button
                    variant="danger"
                    onClick={() => props.onDelete(item, index)}
                  >
                    Delete
                  </Button>{" "}
                </td>
              </tr>
            ))}
          </tbody>
        ) : (
          <tbody>
            {/* if use pagination */}
            {props.data.map((item, index) => (
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.username}</td>
                <td>{item.gender}</td>
                <td>{item.email}</td>
                <td>
                  {item.job.map((job, index) => (
                    <ul key={index}>
                      <li>{job}</li>
                    </ul>
                  ))}
                </td>
                <td>{moment(item.created_date).fromNow()}</td>
                <td>{moment(item.updated_date).fromNow()}</td>
                <td>
                  <Button variant="primary" onClick={() => handleShow(item)}>
                    View
                  </Button>{" "}
                  {show ? (
                    <Modal
                      size="lg"
                      aria-labelledby="contained-modal-title-vcenter"
                      centered
                      show={show}
                      onHide={handleClose}
                    >
                      <Modal.Header closeButton>
                        <Modal.Title>Information</Modal.Title>
                      </Modal.Header>
                      <Modal.Body>
                        <b>Uesrname:</b> <span>{displayDialog.data.username}</span>
                        <br></br>
                        <b>Gmail:</b><span>{displayDialog.data.email}</span> 
                        <br></br>
                        <b>Job:</b><ul className="text-left">
                          {displayDialog.data.job.map((item,index)=>(
                            <li key={index}>{item}</li>
                          ))}
                        </ul>
                        <br></br>
                        <b>Created:</b> <i>{moment(displayDialog.data.created_date).fromNow()}</i>
                        </Modal.Body>
                      <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                          Close
                        </Button>
                      </Modal.Footer>
                    </Modal>
                  ) : (
                    ""
                  )}
                  <Button
                    variant="info"
                    disabled={showPagination}
                    onClick={() => props.onEdit(item, index)}
                  >
                    Update
                  </Button>{" "}
                  <Button
                    variant="danger"
                    onClick={() => props.onDelete(item, index)}
                  >
                    Delete
                  </Button>{" "}
                </td>
              </tr>
            ))}
          </tbody>
        )}
      </Table>
      {showPagination?<Pagination className="d-flex justify-content-center">{items}</Pagination>:""}
    </Col>
  );
}
